<?php
$type = isset($_GET['type']) ? $_GET['type'] : 'redirect';
$allCharts = array( "reviewers", "activeReviewers", "unreviewedArticles", "unreviewedRedirects", "reviewedArticles", "reviewedRedirects", "autoreviewedRedirects");
$name;
switch ($type) {
	case "reviewedArticle":
	case "reviewedArticles":
		$name = "reviewedArticles";
		break;
	case "reviewedRedirect":
	case "reviewedRedirects":
		$name = "reviewedRedirects";
    	break;
	case "activeReviewer":
	case "activeReviewers":
		$name = "activeReviewers";
		break;
	case "article":
	case "articles":
		$name = "unreviewedArticles";
		break;
	case "autoreviewedRedirects":
		$name = "autoreviewedRedirects";
		break;
	case "redirect":
	case "redirects":
	default:
		$name = "unreviewedRedirects";
}
?>
<head>
<style>
body {
  font-family: Roboto, sans-serif;
}

#chart {
  max-width: 650px;
  margin: 35px auto;
}
#links {
  font-size: small;
}
</style>

<script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/apexcharts/3.37.0/apexcharts.js"></script>
<script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/3.6.3/jquery.min.js"></script>

</head>

<body>
<div id="links">
<b>Reports:</b>
<?php
	foreach ($allCharts as $chart) {
		if ( $type === $chart) {
			echo " " . $chart;
		} else {
			echo " <a href='chart.php?type=$chart'>$chart</a>";
		}
	}
?>
</div>
<div id="chart">
</div>
<script>
var options = {
	chart: {
		type: 'line'
	},
	series: [],
	title: {
		text: 'NPP Stats',
	},
	noData: {
		text: 'Loading...'
	}
	}

var chart = new ApexCharts(document.querySelector("#chart"), options);
chart.render();

var url = 'data.php?type=<?=$name?>';

$.getJSON(url, function(response) {
  chart.updateSeries([{
    name: '<?=$name?>',
    data: response
  }])
});

</script>
<?php
        if ( in_array( $type, array( "reviewer", "reviewers")  ) ) {
                echo '<a href="https://en.wikipedia.org/wiki/Special:Log?type=rights&user=&page=&wpdate=&tagfilter=&subtype=rights&wpFormIdentifier=logeventslist"><p style="text-align:center">Permission Log</p></a>';
        }
?>
<div style="float:right;"><img src="images/CC-0.png"/></div>
<body>
