<?php
$type = isset($_GET['type']) ? $_GET['type'] : 'unreviewedRedirects';
$ts_pw = posix_getpwuid(posix_getuid());
$ts_mycnf = parse_ini_file($ts_pw['dir'] . "/.my.cnf");
$mysqli = new mysqli('tools.db.svc.wikimedia.cloud', $ts_mycnf['user'], $ts_mycnf['password'], 's55221__nppstats_p');
unset($ts_mycnf, $ts_pw);

$stmt = $mysqli->prepare('select date(moment) as x, max(value) as y from npp_stats where name="'.$type.'" and moment> DATE_ADD(CURDATE(), INTERVAL -182 DAY) group by date(moment) order by x');
$stmt->execute();
$result = $stmt->get_result();

$myArray = array();
while($row = $result->fetch_assoc()) {
	$myArray[] = $row;
}
header('Content-Type: application/json; charset=utf-8');
print_r( json_encode($myArray) );
?>

