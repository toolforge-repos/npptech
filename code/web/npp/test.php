<?php
$ts_pw = posix_getpwuid(posix_getuid());
$ts_mycnf = parse_ini_file($ts_pw['dir'] . "/.my.cnf");
$mysqli = new mysqli('tools.db.svc.wikimedia.cloud', $ts_mycnf['user'], $ts_mycnf['password'], 's55221__nppstats_p');
unset($ts_mycnf, $ts_pw);

$stmt = $mysqli->prepare('select * from npp_stats order by created_on desc limit 10');
$stmt->execute();
$result = $stmt->get_result();
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://tools-static.wmflabs.org/cdnjs/ajax/libs/bootstrap/5.2.3/css/bootstrap.min.css">
</head>
<body>
<div class="container">
 <div class="row">
   <div class="col-sm-8">
    <div class="table-responsive">
      <table class="table table-bordered">
       <thead><tr><th>S.N</th>
         <th>Name</th>
         <th>Moment</th>
         <th>Value</th>
    </thead>
    <tbody>
  <?php
      if($result->num_rows > 0){      
      $sn=1;
      while($data = $result->fetch_assoc()){
    ?>
      <tr>
      <td><?php echo $sn; ?></td>
      <td><?php echo $data['name']??''; ?></td>
      <td><?php echo $data['moment']??''; ?></td>
      <td><?php echo $data['value']??''; ?></td>
     </tr>
     <?php
      $sn++;}}else{ ?>
      <tr>
        <td colspan="8">
    <?php print_r($fetchData); ?>
  </td>
    <tr>
    <?php
    }?>
    </tbody>
     </table>
   </div>
</div>
</div>
</div>
</body>
</html>
