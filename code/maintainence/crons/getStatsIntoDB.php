#!/usr/bin/php
<?php
$arguments = getopt("t:");
$mysqli = connectToDB();
if ( !isset( $arguments["t"] ) ) {
	$arguments["t"] ="";
}

switch ( $arguments["t"] ) {
	case "t": //test
		getPseudoAutoreviewedStats();
		break;
	case "w": //weekly
		break;
	case "d": //daily
		getReviewedStats();
		getAutoreviewedStats();
		getPseudoAutoreviewedStats();
		break;
	case "h": //hourly
	default:
		getCurrentUnreviewedStats();
		break;
}

function connectToDB( $server = 'tools.db.svc.wikimedia.cloud', $db = 's55221__nppstats_p' ) {
	// Check connection
	$ts_pw = posix_getpwuid( posix_getuid() );
	$ts_mycnf = parse_ini_file( $ts_pw['dir'] . "/.my.cnf" );
	$mysqli = new mysqli( $server, $ts_mycnf['user'], $ts_mycnf['password'], $db );
	unset( $ts_mycnf, $ts_pw );
	if ( mysqli_connect_errno() ) {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  exit();
	}
	return $mysqli;
}

function insertIntoNPPStats( $name, $moment, $value ) {
	global $mysqli;
	$stmt = $mysqli->prepare('insert into `npp_stats`(name, moment, value) VALUES (?,?,?)');
	$stmt -> bind_param( "ssi", $name, $moment, $value );
	print_r($stmt);
	
	$stmt->execute();
//	$result = $stmt->get_result();
//	print_r($result);
//	return $result;
}

function getCurrentUnreviewedStats() {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, 'https://en.wikipedia.org/w/api.php?action=pagetriagestats&format=json' );
        $response = json_decode(curl_exec($ch), true);

	$unreviewedArticles = $response['pagetriagestats']['stats']['unreviewedarticle']['count'];
	$unreviewedRedirects = $response['pagetriagestats']['stats']['unreviewedredirect']['count'];

	insertIntoNPPStats( "unreviewedArticles", date( 'Y-m-d H:i:s', time() ), $unreviewedArticles );
	insertIntoNPPStats( "unreviewedRedirects", date( 'Y-m-d H:i:s', time() ), $unreviewedRedirects );
}

function getReviewedStats() {

	$ts_pw = posix_getpwuid( posix_getuid() );
	$ts_mycnf = parse_ini_file( $ts_pw['dir'] . "/.my.cnf" );
	$mysqlReplica = new mysqli( "enwiki.analytics.db.svc.wikimedia.cloud", $ts_mycnf['user'], $ts_mycnf['password'], "enwiki_p" );
	unset( $ts_mycnf, $ts_pw );

	$yesterday = date( 'Ymd000000', time() - 86400 );
	echo $yesterday."\n";

	$query = "select review_date as date,
      COUNT(IF(logtemp.page_is_redirect = 0, 1, NULL)) as `Articles`,
      COUNT(IF(logtemp.page_is_redirect = 1, 1, NULL)) as `Redirects`
from (
	SELECT distinct page_id, left(log_timestamp,8) as review_date, page_is_redirect
    FROM logging_userindex JOIN page ON page_title = log_title AND page_namespace = log_namespace
    WHERE log_timestamp >= $yesterday AND ((log_type = 'patrol' AND log_action = 'patrol') OR
         (log_type = 'pagetriage-curation' AND log_action = 'reviewed')) AND log_namespace=0
) logtemp group by review_date having review_date=left('$yesterday',8)";

	$result = $mysqlReplica->query( $query );
	$row = $result->fetch_assoc();

	insertIntoNPPStats( "reviewedArticles", date( 'Y-m-d 23:59:59', time() - 86400 ), $row['Articles'] );
	insertIntoNPPStats( "reviewedRedirects", date( 'Y-m-d 23:59:59', time() - 86400 ), $row['Redirects'] );
}

function getAutoreviewedStats() {

	$ts_pw = posix_getpwuid( posix_getuid() );
	$ts_mycnf = parse_ini_file( $ts_pw['dir'] . "/.my.cnf" );
	$mysqlReplica = new mysqli( "enwiki.analytics.db.svc.wikimedia.cloud", $ts_mycnf['user'], $ts_mycnf['password'], "enwiki_p" );
	unset( $ts_mycnf, $ts_pw );

	$yesterday = date( 'Ymd', time() - 86400 );

	$query = "select left(review_date, 8) as date,
      COUNT(IF(logtemp.page_is_redirect = 0, 1, NULL)) as `Articles`,
      COUNT(IF(logtemp.page_is_redirect = 1, 1, NULL)) as `Redirects`
from (
	SELECT distinct page_id, left(ptrp_reviewed_updated, 8) as review_date, page_is_redirect
		FROM pagetriage_page 
		JOIN page on ptrp_page_id=page_id and page_namespace=0
		WHERE ptrp_reviewed=3
) logtemp group by review_date having review_date='$yesterday'";

	$result = $mysqlReplica->query( $query );
	$row = $result->fetch_assoc();

	insertIntoNPPStats( "autoreviewedArticles", date( 'Y-m-d 23:59:59', time() - 86400 ), $row['Articles'] );
	insertIntoNPPStats( "autoreviewedRedirects", date( 'Y-m-d 23:59:59', time() - 86400 ), $row['Redirects'] );
}


function getPseudoAutoreviewedStats() {

	$ts_pw = posix_getpwuid( posix_getuid() );
	$ts_mycnf = parse_ini_file( $ts_pw['dir'] . "/.my.cnf" );
	$mysqlReplica = new mysqli( "enwiki.analytics.db.svc.wikimedia.cloud", $ts_mycnf['user'], $ts_mycnf['password'], "enwiki_p" );
	unset( $ts_mycnf, $ts_pw );

	$yesterday = date( 'Ymd', time() - 86400 );

	// DannyS712 bot III 's reviews
	$query = "SELECT count(*) as Redirects
		FROM pagetriage_page 
		JOIN page ON ptrp_page_id=page_id and page_namespace=0
        WHERE ptrp_reviewed=1 and page_is_redirect=1 
        and ptrp_last_reviewed_by=36553370 
		and left(ptrp_reviewed_updated, 8)='$yesterday'";

	$result = $mysqlReplica->query( $query );
	$row = $result->fetch_assoc();

	insertIntoNPPStats( "pseudoAutoreviewedRedirects", date( 'Y-m-d 23:59:59', time() - 86400 ), $row['Redirects'] );
}

